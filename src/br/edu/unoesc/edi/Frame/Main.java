/**
 * @author Thiago F. e Leonardo l.
 * @version 1.2
 */
package br.edu.unoesc.edi.Frame;

import java.awt.EventQueue;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class Main extends Application {
	/**
	 * classe que chama a janela sobre do JavaFX
	 */
	@Override
	public void start(Stage primaryStage) throws IOException {
		Pane root = FXMLLoader.load(getClass().getResource("Janela.fxml"));
		Scene scene = new Scene(root, 400, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void Sobre(String[] args) {
		launch(args);
		

	}
}
