/**
 * @author Thago F. e Leonardo L.
 * @version 2.0
 */
package br.edu.unoesc.edi.Frame;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import br.edu.unoesc.edi.move.Move;
import br.edu.unoesc.edi.util.MoveEsp;


public class Janela extends JFrame {
	/**
	 * Classe que mostra a janela principal
	 */

	protected static final UIManager UIMananger = null;
	private JPanel contentPane;
	private JTextField txtT;
	private JTextField txtF;
	public static String localTxt;
	public static String par;
	public static boolean nome = false;
	public static int ver;
	private JTextField Borda;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	
	
	

	
	public static void main(String[] args) {
		/**
		 * metodo main
		 */
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Janela frame = new Janela();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	

	/**
	 * cria o frame.
	 */
	public Janela() {
		/**
		 * metodo com a funcionalidade da janela
		 */
		setForeground(Color.BLACK);
		setTitle("ReOrganizing");
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Janela.class.getResource("/br/edu/unoesc/edi/imgs/Icone.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 402, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtT = new JTextField();
		txtT.setText(" ");
		txtT.setBackground(Color.WHITE);
		txtT.setEditable(false);
		txtT.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtT.setBounds(10, 75, 366, 35);
		contentPane.add(txtT);
		txtT.setColumns(10);
		
		JCheckBox check = new JCheckBox("Nome especifico");
		check.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		check.setFocusPainted(false);
		check.setFocusTraversalKeysEnabled(false);
		check.setBackground(Color.WHITE);
		check.setFont(new Font("Tahoma", Font.PLAIN, 14));
		check.setBounds(6, 144, 135, 30);
		contentPane.add(check);
		
				JButton btnNewButton = new JButton("Organizar");
				btnNewButton.setForeground(Color.WHITE);
				btnNewButton.setFocusPainted(false);
				btnNewButton.setFocusable(false);
				btnNewButton.setFocusTraversalKeysEnabled(false);
				btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
				btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				btnNewButton.setBackground(Color.GRAY);
				btnNewButton.setBorder(null);
				btnNewButton.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent arg0) {
						
						localTxt = txtT.getText();
						//Esse if serve para verificar se a caixa de texto aonde cont�m 
						//o caminho da pasta esta vazia
						if (localTxt.equals(" ")) {
							JOptionPane.showMessageDialog(null, "� necess�rio selecionar um local antes de organizar", "ERRO", JOptionPane.WARNING_MESSAGE);
						}else {
							
						if (check.isSelected()) {
							par = txtF.getText();
							par = par.trim();
							nome = true;
							MoveEsp chama = new MoveEsp();
							chama.moveEsp();
							
						} else {
							Move chama = new Move();
							chama.Move();
							}
						JOptionPane.showMessageDialog(null, "Arquivos organizados", "ORGANIZADO", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});
				
				
				btnNewButton.setBounds(271, 221, 105, 29);
				contentPane.add(btnNewButton);

		txtF = new JTextField();
		txtF.setBackground(Color.WHITE);
		txtF.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtF.setBounds(147, 145, 229, 29);
		contentPane.add(txtF);
		txtF.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(Janela.class.getResource("/br/edu/unoesc/edi/imgs/Search.png")));
		btnNewButton_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton_1.setBorder(null);
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setFocusPainted(false);
		btnNewButton_1.setFocusTraversalKeysEnabled(false);
		btnNewButton_1.setFocusable(false);
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JFileChooser escolhe = new JFileChooser();
				escolhe.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int ax = escolhe.showOpenDialog(null);
				if (ax == JFileChooser.APPROVE_OPTION) {
					txtT.setText(escolhe.getSelectedFile().getAbsolutePath());
					
				}
			}
		});
		btnNewButton_1.setBounds(10, 11, 366, 53);
		contentPane.add(btnNewButton_1);
				
				Borda = new JTextField();
				Borda.setEnabled(false);
				Borda.setBorder(null);
				Borda.setEditable(false);
				Borda.setSelectedTextColor(Color.LIGHT_GRAY);
				Borda.setSelectionColor(Color.LIGHT_GRAY);
				Borda.setAutoscrolls(false);
				Borda.setFocusable(false);
				Borda.setFocusTraversalKeysEnabled(false);
				Borda.setBounds(0, 229, 271, 11);
				contentPane.add(Borda);
				Borda.setColumns(10);
				
				textField = new JTextField();
				textField.setSelectionColor(Color.LIGHT_GRAY);
				textField.setSelectedTextColor(Color.LIGHT_GRAY);
				textField.setFocusable(false);
				textField.setFocusTraversalKeysEnabled(false);
				textField.setEnabled(false);
				textField.setEditable(false);
				textField.setColumns(10);
				textField.setBorder(null);
				textField.setAutoscrolls(false);
				textField.setBounds(376, 231, 10, 11);
				contentPane.add(textField);
				
				JButton btnNewButton_2 = new JButton("...");
				btnNewButton_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				btnNewButton_2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Main chama= new Main();
						chama.Sobre(null);
						
					}
				});
				btnNewButton_2.setBackground(Color.WHITE);
				btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
				btnNewButton_2.setFocusable(false);
				btnNewButton_2.setFocusTraversalKeysEnabled(false);
				btnNewButton_2.setFocusPainted(false);
				btnNewButton_2.setBorder(null);
				btnNewButton_2.setBounds(0, 240, 22, 23);
				contentPane.add(btnNewButton_2);
				
	

	}
}
