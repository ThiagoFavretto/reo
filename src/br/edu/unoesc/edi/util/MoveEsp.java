/**
 * @author Thiago F. e Leonardo L.
 * @version 2.0
 */
package br.edu.unoesc.edi.util;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import br.edu.unoesc.edi.Frame.Janela;

public class MoveEsp {
	/**
	 * classe que move arquivos que comtem um nome especifico
	 */

	public void moveEsp() {
		/**
		 * metodo que move arquivos que comtem um nome especifico
		 */
		String local = Janela.localTxt;
		String nome = Janela.par;

		Set<String> tipos = new HashSet<String>();

		// le a pasta ande vamos trabalhar
		// String local = ler.nextLine();
		String vereficacao, vereficacao2;
		System.out.println(local);
		// lista os arquivos e extensoes de tudo q tem dentro (pastas nao tem extensoes
		// entao bas excluir todos os outro q tem extoes q restara so pastas))
		File arquivo1 = new File(local);
		File conteudo[] = arquivo1.listFiles();
		String[] arquivos = new String[conteudo.length];

		int i = 0;
		// pega o nome + a extensao dos arquivos dentro da pasta
		for (int j = conteudo.length; i < j; i++) {

			arquivos[i] = (conteudo[i].getName());
			tipos.add(arquivos[i].substring((arquivos[i].lastIndexOf(".") + 1), arquivos[i].length()));
		}

		System.out.println(tipos);
		System.out.println(tipos.size());
		Iterator<String> iterator = tipos.iterator();
		while (iterator.hasNext()) {
			String extensao = iterator.next();

			try {
				for (int j = 0; j < arquivos.length; j++) {
					vereficacao = (nome);
					vereficacao2 =(" "+nome+".docx");

					if (((arquivos[j].contains(vereficacao) || (arquivos[j].startsWith(vereficacao))) || arquivos[j].endsWith(vereficacao2))) {
						File arquivo = new File(local + "\\" + arquivos[j]);
						File diretorio = new File(local + "\\" + nome);
						diretorio.mkdir();
					}
				}
			} catch (Exception ex) {
				System.out.println("Nao foi posivel criar a pasta " + extensao);
			}

			for (int j = 0; j < arquivos.length; j++) {
				if (arquivos[j].endsWith(extensao)) {
					// nome do arquivo
					System.out.println(arquivos[j]);

					vereficacao = (" " + nome + " ");

					if ((arquivos[j].contains(vereficacao)) || (arquivos[j].startsWith(vereficacao.trim()))) {
						File arquivo = new File(local + "\\" + arquivos[j]);

						if (!arquivo.exists()) {
							System.out.println("Arquivo n�o encontrado");
						} else {

							// Diretorio de destino
							File diretorioDestino = new File(local + "\\" + nome);

							// Move o arquivo para o novo diretorio
							boolean sucesso = arquivo.renameTo(new File(diretorioDestino, arquivo.getName()));
							if (!sucesso) {
								System.out.println("Erro ao mover arquivo '" + arquivo.getAbsolutePath() + "' para '"
										+ diretorioDestino.getAbsolutePath() + "'");
							}

						}
					} else {

					}
				}

			}
		}

	}
}
