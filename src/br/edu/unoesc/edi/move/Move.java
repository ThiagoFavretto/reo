/**
	 * @author Thiago F. e Leonardo L.
	 * @version 1.0
	 */
package br.edu.unoesc.edi.move;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import br.edu.unoesc.edi.Frame.Janela;

public class Move {
	/**
	 * Classe que move todos os arquivos
	 */
	


	public void Move() {
		/**
		 * metodo para mover todos os arquivos
		 */

		Set<String> tipos = new HashSet<String>();

		// le a pasta ande vamos trabalhar
		// String local = ler.nextLine();
		String local = Janela.localTxt;

		System.out.println(local);
		// lista os arquivos e extensoes de tudo q tem dentro (pastas nao tem extensoes
		// entao bas excluir todos os outro q tem extoes q restara so pastas))
		File arquivo1 = new File(local);
		File conteudo[] = arquivo1.listFiles();
		String[] arquivos = new String[conteudo.length];

		int i = 0;
		// pega o nome + a extensao dos arquivos dentro da pasta
		for (int j = conteudo.length; i < j; i++) {

			arquivos[i] = (conteudo[i].getName());
			tipos.add(arquivos[i].substring((arquivos[i].lastIndexOf(".") + 1), arquivos[i].length()));
		}

		// C:\Users\User\Desktop\arquivos teste para o filtro -------local tes
		// C:\Users\322244\Desktop\c

		System.out.println(tipos);
		System.out.println(tipos.size());
		Iterator<String> iterator = tipos.iterator();
		while (iterator.hasNext()) {
			String extensao = iterator.next();

			try {
				File diretorio = new File(local + "\\" + extensao);
				diretorio.mkdir();
			} catch (Exception ex) {
				System.out.println("Nao foi posivel criar a pasta " + extensao);
			}

			for (int j = 0; j < arquivos.length; j++) {
				if (arquivos[j].endsWith(extensao)) {
					// nome do arquivo
					System.out.println(arquivos[j]);

					File arquivo = new File(local + "\\" + arquivos[j]);

					if (!arquivo.exists()) {
						System.out.println("Arquivo n�o encontrado");
					} else {

						// Diretorio de destino
						File diretorioDestino = new File(local + "\\" + extensao);

						// Move o arquivo para o novo diretorio
						boolean sucesso = arquivo.renameTo(new File(diretorioDestino, arquivo.getName()));
						if (!sucesso) {
							System.out.println("Erro ao mover arquivo '" + arquivo.getAbsolutePath() + "' para '"
									+ diretorioDestino.getAbsolutePath() + "'");
						}

					}
				}

			}

		}
		//
	}

}


